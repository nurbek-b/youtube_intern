# Web App to download and convert Youtube file to MP3

### Application which helps user in easy way to download any Youtube video to MP3 

* Clone this repository
```bash
git clone git@gitlab.com:NurbekAka/youtube_intern.git
```


* Activate virtual environment:
```
pip install pipenv
pipenv --python 3
pipenv shell
pipenv install
```

* Create private `.env` file inside of project directory. Copy all data from `.env_example` and paste inside of `.env` file. **Note**: Change the values of secrets to yours. 

* Activate `redis-server` in new Terminal

* Open new Terminal. Run the following command from your project root:
 `celery -A converter.celery worker -l DEBUG -E`

* Finally,in 3rd terminal run project with command: `python3 manage.py runserver`

_______________________

