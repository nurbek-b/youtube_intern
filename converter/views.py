from __future__ import unicode_literals

import os
import sys
from .task import *
from datetime import datetime
from .models import Downloader
from .forms import DownloadForm
from django.shortcuts import render


def main_page(request):
    if request.method == 'POST':
        form = DownloadForm(request.POST)
        if form.is_valid():
            video_url = form.cleaned_data.get('link')
            email = form.cleaned_data.get('email')
            download_url = request.get_host()
            try:
                convert_load.delay(video_url, email, download_url)
                Downloader.objects.create(url=video_url, email=email)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' Type:{} Filename:{} Line:{}ERROR: {}'.
                      format(exc_type, fname, exc_tb.tb_lineno, e))
        return render(request, 'download_link.html')
    else:
        form = DownloadForm()
        return render(request, 'main_page.html', {'form': form})


def history(request):
    return render(request, 'history.html', {'instance': Downloader.objects.all()})


def download_link(request):
    return render(request, 'download_link.html', {})
