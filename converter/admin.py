from django.contrib import admin
from .models import Downloader


@admin.register(Downloader)
class DownloadingAdmin(admin.ModelAdmin):
    list_display = ('url', 'email', 'loaded_in',)
    list_filter = ('loaded_in', 'email')




