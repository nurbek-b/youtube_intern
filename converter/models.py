from django.db import models


class Downloader(models.Model):
    url = models.URLField(max_length=300, verbose_name='download_url')
    email = models.EmailField(verbose_name='email_to')
    loaded_in = models.DateTimeField(auto_now_add=True, verbose_name='loaded_in')

    def __str__(self):
        return self.url



