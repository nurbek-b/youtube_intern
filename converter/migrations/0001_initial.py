# Generated by Django 2.2.1 on 2019-05-16 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Downloader',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('given_url', models.URLField(max_length=300)),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
    ]
