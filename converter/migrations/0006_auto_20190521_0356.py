# Generated by Django 2.2.1 on 2019-05-20 21:56

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('converter', '0005_auto_20190521_0140'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='downloader',
            name='given_url',
        ),
        migrations.AddField(
            model_name='downloader',
            name='url',
            field=models.URLField(default=django.utils.timezone.now, max_length=300, verbose_name='download_url'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='downloader',
            name='email',
            field=models.EmailField(default=3, max_length=254, verbose_name='email_to'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='downloader',
            name='loaded_in',
            field=models.DateTimeField(auto_now_add=True, verbose_name='loaded_in'),
        ),
    ]
