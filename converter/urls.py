from django.urls import path
from . import views


urlpatterns = [
    path('', views.main_page, name='index'),
    path('link/', views.download_link, name='link_download'),
    path('history/', views.history, name='history'),
    ]

