from django import forms
from .models import Downloader


class DownloadForm(forms.ModelForm):
    link = forms.RegexField(
        regex=r'^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$',
        widget=forms.TextInput(attrs={'placeholder': 'Paste your Youtube link'}), max_length=300)

    class Meta:
        model = Downloader
        fields = ('link', 'email')