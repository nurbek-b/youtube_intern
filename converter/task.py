from __future__ import unicode_literals
import youtube_dl
from celery import task
from django.conf.global_settings import EMAIL_HOST_USER
from django.core.mail import send_mail


@task
def convert_load(video_url, email, download_url):
    # Mp3 format options
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': 'media/%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192'}
        ],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as yld:
        file_data = yld.extract_info(video_url)
        title = file_data['title']
    send_mail(
        'Download link',
        ('http://' + download_url + "/media/" + title).replace(" ", "%20") + '.mp3',
        EMAIL_HOST_USER,
        [email],
        fail_silently=True,
        )

